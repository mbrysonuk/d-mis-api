<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('first_name')->nullable();
            $table->string('surname')->nullable();
            $table->string('date_of_birth')->nullable();
            
            $table->string('address_line_1')->nullable();
            $table->string('address_line_2')->nullable();
            $table->string('address_line_3')->nullable();
            $table->string('postcode')->nullable();
            $table->string('country')->nullable();
            
            $table->string('tel')->nullable();
            $table->string('mobile_tel')->nullable();
            $table->string('email')->nullable();
            $table->string('ethnicity')->nullable();
            $table->string('offering_1')->nullable();
            $table->string('last_school')->nullable();
            $table->text('qualifications')->nullable();
            
            $table->boolean('have_employer')->nullable();
            $table->string('employer_name')->nullable();
            $table->string('employer_email')->nullable();
            $table->string('employer_tel')->nullable();

            $table->string('referee_1_name')->nullable();
            $table->string('referee_1_relationship')->nullable();
            $table->string('referee_1_email')->nullable();
            $table->string('referee_1_tel')->nullable();
            $table->string('referee_2_name')->nullable();
            $table->string('referee_2_relationship')->nullable();
            $table->string('referee_2_email')->nullable();
            $table->string('referee_2_tel')->nullable();

            $table->boolean('interview_support')->nullable();
            $table->boolean('learning_support')->nullable();
            $table->boolean('any_other_support')->nullable();
            $table->text('any_other_support_details')->nullable();
            $table->boolean('local_authority_care')->nullable();
            $table->boolean('further_support_info')->nullable();

            //required fields
            $table->string('request_date')->nullable();
            $table->string('request_status')->nullable();
            $table->string('academic_year_id')->nullable();
            $table->string('is_full_time')->nullable();
            $table->string('sent_marketing_info')->nullable()->default('false');

            $table->string('sent_to_ps')->default(false)->nullable();
            $table->string('attempts')->default(0)->nullable();
            $table->string('ps_application_id')->nullable();
            $table->string('site_application_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}