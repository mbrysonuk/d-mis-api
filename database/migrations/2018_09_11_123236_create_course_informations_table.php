<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_information_id')->nullable();
            $table->string('course_info_image_type')->nullable();
            $table->string('course_info_text_type')->nullable();
            $table->string('course_info_description')->nullable();
            $table->binary('course_image')->nullable();
            $table->text('course_text')->nullable();
            $table->string('academic_year_id')->nullable();
            $table->string('course_level')->nullable();
            $table->string('course_area')->nullable();
            $table->string('parent_course_area')->nullable();
            $table->string('categories')->nullable();
            $table->integer('sort_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_information');
    }
}
