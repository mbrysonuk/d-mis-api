<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('offering_id')->nullable();
            $table->string('name')->nullable();
            $table->string('clean_name')->nullable();
            $table->string('slug')->nullable();
            $table->string('qual_id')->nullable();
            $table->string('code')->nullable();
            $table->string('course_type')->nullable();
            $table->string('awarding_body_id')->nullable();
            $table->string('academic_year_id')->nullable();
            $table->string('offering_status')->nullable();
            $table->string('course_level')->nullable();
            $table->string('course_area')->nullable();
            $table->string('parent_course_area')->nullable();
            $table->string('duration')->nullable();
            $table->string('glh')->nullable();
            $table->string('annual_glh')->nullable();
            $table->string('weekly_hours')->nullable();
            $table->string('number_of_weeks')->nullable();
            $table->string('study_year')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('offering_type')->nullable();
            $table->string('site_name')->nullable();
            $table->string('fee_type_name')->nullable();
            $table->string('amount')->nullable();
            $table->string('course_information_id')->nullable();
            $table->string('university_link')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_desc')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
