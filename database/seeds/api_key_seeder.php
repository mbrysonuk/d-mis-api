<?php

use Illuminate\Database\Seeder;

class api_key_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('api_keys')->insert(

			[	
				'key'		=> '1737d911-a662-47bf-a3ca-201ec1fe28cb',
				'scope'		=> '1',
				'enabled' 	=> 1
			]

        );
    }
}
