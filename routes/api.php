<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Course Routes
Route::get('/courses/getcourse/{offering_id}', 'CourseController@get_course')->middleware('CustomAPIAuth');
Route::get('/courses/getcourses/{category_id}', 'CourseController@get_courses')->middleware('CustomAPIAuth');

//Application Routes
Route::post('/application/post/', 'ApplicationController@store')->middleware('CustomAPIAuth');