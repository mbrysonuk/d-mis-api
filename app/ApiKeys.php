<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiKeys extends Model
{	
	
	//api key generator. Not used currently. Intention was to have this generate keys on a regular basis and expire old ones, for security.
    private function create_key() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 30; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
    }
}
