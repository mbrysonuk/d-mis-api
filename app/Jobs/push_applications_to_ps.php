<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\application;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;

class push_applications_to_ps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach(application::where('sent_to_ps', false)->get() as $application) {
            
            //get the next unsent application
            //$application = application::where('sent_to_ps', false)->first();

            //var_dump($application);
            //die();
            //see if the application has failed more than 4 times
            //skip it if so to avoid all applications been delayed.
            if($application->attempts > 4) {
                echo 'over 4 attempts made, skipping & emails';
                //skip
                continue;
            }

            //start the value construct
            $values = "('";

            //Split the postcode into two parts. its a ProSolution requirement
            $postcode = str_replace(' ', '',$application->postcode);
            if(strlen($postcode) == 6) {
                $postcode_out   = substr($postcode,0,3);
                $postcode_in    = substr($postcode,3,10);
            }else{
                $postcode_out   = substr($postcode,0,4);
                $postcode_in    = substr($postcode,4,10);
            }

            //Add the values to the values variable    
            $values = $values .
                $application->title . "','" .
                $application->first_name . "','" .
                $application->surname . "','" .
                $application->date_of_birth . "','" .
                $application->address_line_1 . "','" .
                $application->address_line_2 . "','" .
                $application->address_line_3 . "','" .
                $postcode_out . "','" .
                $postcode_in . "','" .
                $application->country . "','" .
                $application->tel . "','" .
                $application->mobile_tel . "','" .
                $application->email . "','" .
                $application->ethnicity . "','" .
                $application->offering_1 . "','" .
                $application->last_school . "','" .

                //EMPLOYER DETAILS
                $application->employer_name . "','" .
                //NOWHERE TO STORE THIS $application->employer_email . "','" .
                $application->employer_tel . "','" .

                //the below are required fields
                $application->request_date . "','" .
                $application->request_status . "','" .
                $application->academic_year_id . "','" .
                $application->is_full_time . "','" .
                $application->sent_marketing_info . "','" .

                //the below are custom fields used to trigger emails in PS
                $application->interview_support . "','" .
                $application->learning_support . "','" .
                $application->any_other_support . "','" .
                $application->any_other_support_details . "','" .
                $application->local_authority_care . "','" .
                $application->further_support_info . "','" .

                //PUSH REFEREES INTO REFERENCE NOTES AS ONE FIELD
                $application->referee_1_name . ", " .
                $application->referee_1_relationship . ", " .
                $application->referee_1_email . ", " .
                $application->referee_1_tel . ", " . "|" .

                $application->referee_2_name . ", " .
                $application->referee_2_relationship . ", " .
                $application->referee_2_email . ", " .
                $application->referee_2_tel;

            $values = $values . "')";

            //Construct the SQL query, and return the application request ID.
            $sql = "

            INSERT INTO [dbo].[ApplicationRequest]
            (
                [Title],
                [FirstForename],
                [Surname],
                [DateOfBirth],
                [Address1],
                [Address2],
                [Address3],
                [PostcodeOut],
                [PostcodeIn],
                [Country],
                [Tel],
                [MobileTel],
                [Email],
                [EthnicGroupID],
                [Offering1ID],
                [LastSchool],

                [EmployerName],
                [EmployerTel],

                [RequestDate],
                [RequestStatus],
                [AcademicYearID],
                [IsFullTime],
                [SentMarketingInfo],

                [StudentDetailUserDefined30],
                [StudentDetailUserDefined31],
                [StudentDetailUserDefined32],
                [StudentDetailUserDefined33],
                [StudentDetailUserDefined34],
                [StudentDetailUserDefined35],

                [ReferencesNotes]

            )
            VALUES " . $values . "
            " ;


            echo PHP_EOL;
            try {
                echo 'Pushing application...';

                //connect to PSWebEnrolemnt DB and run insert query.
                $application_push = DB::connection('mssql_sqlsrv');
                $application_push->insert($sql);

                //grab the newly inserted record using scope identity for later use when pushing the qualifications.
                $psapplication_id = DB::connection('mssql_sqlsrv')->select("SELECT [ApplicationRequestID] FROM [dbo].[ApplicationRequest] WHERE [ApplicationRequestID] = SCOPE_IDENTITY();");
                
                //store the application id in local table (we need it for pushing qualifications!)
                $application->ps_application_id = $psapplication_id[0]->ApplicationRequestID;
                $application->save();

                //grab the qualifications from the application.
                //also, initialise the qual results SQL values variable. Stops the function returning an error if no quals exist.
                $qualifications = json_decode($application->qualifications, true);
                $qual_result = '';

                //loop through each qualification
                foreach($qualifications as $qualification) {
                    //check to see if the qualification entries first two fields are empty. Dont bother submitting them if they are.
                    if(!empty($qualification[0])) {
                        
                        if(empty($qual_result)) {
                        	$qual_result = "('";
                        }else{
                        	$qual_result = $qual_result .",('";
                        }

                        $qual_result =  $qual_result . 
    	                    $application->ps_application_id ."','" .
    	                    $qualification[0] ."','" .
    	                    $qualification[1] ."','" .
    	                    $qualification[2] ."','" .
    	                    $qualification[3];

                        $qual_result =  $qual_result . "')";

                    }//endemptyifcheck
                }//endfor

                echo 'Pushing qualifications...';

                //only attempt to push qualifications if they exist!
                if(!empty($qual_result) ) {
                    $qual_sql = "
                        INSERT INTO [dbo].[ApplicationRequestQualsOnEntry]
                        (
                            [ApplicationRequestID],
                            [Subject],
                            [Level],
                            [Grade],
                            [DateAwarded]
                        )
                        VALUES
                        " . $qual_result;
                        var_dump($qual_sql);
                        echo 'running quals';
                    $psapplication_id = DB::connection('mssql_sqlsrv')->insert($qual_sql);
                }

                //if the application sent, set the sent variable to true
                if($application->ps_application_id != 0) {
                    $application->sent_to_ps = true;    
                    $application->save();
                }
                

            } catch (\Exception $e) {
                //if we have a failure, store the attempt number.
                echo 'FAILURE' . PHP_EOL;
                $application->attempts = $application->attempts + 1;
                $application->save();

                Mail::raw("Urgent Action Required!\n\nThere has been an issue with the application request push to ProSolution. Please login and review.", function ($message) {
                    $message->to('development@digitalallies.co.uk');
                    $message->subject('CRITICAL FAILURE. ACTION REQUIRED - Darlington College API');
                });

                echo ('Could not establish connection to DB. Please review information.' . PHP_EOL);
            }

            echo 'Next....';

        }//endwhile
    }
}
