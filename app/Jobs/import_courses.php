<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;

class import_courses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    //get all courses from Pro Solution and store them locally
    //Values are echoed for use in console to track progress of function as it runs.
    public function handle()
    {
        echo 'Starting...'. PHP_EOL;
        //Construct the SQL query
        $sql = "

        SELECT 
            [dcol_offering].[OfferingID] AS [offering_id],
            [dcol_offering].[Name] AS [name],
            [dcol_course_info].[Description] AS [clean_name],
            -- [dcol_offering].[QualID] AS [qual_id],
            [dcol_qualification].[LEARNING_AIM_TITLE] AS [qual_id],
            [dcol_offering].[Code] AS [code],
            -- [dcol_offering].[AwardingBodyID] AS [awarding_body_id],
            [dcol_awarding_body].[FullName] AS [awarding_body_id],
            [dcol_offering].[AcademicYearID] AS [academic_year_id],
            [dcol_offering_status].[Description] AS [offering_status],
            -- [dcol_subject].[LevelNum] AS [course_level],
            [dcol_subject].[Name] AS [course_area],
            [dcol_parent_subject].[Name] AS [parent_course_area],
            [dcol_offering].[Duration] AS [duration],
            [dcol_offering].[GLH] AS [glh],
            [dcol_offering].[AnnualGLH] AS [annual_glh],
            [dcol_offering].[WeeklyHours] AS [weekly_hours],
            [dcol_offering].[NumberOfWeeks] AS [number_of_weeks],
            [dcol_offering].[StudyYear] AS [study_year],
            [dcol_offering].[StartDate] AS [start_date],
            [dcol_offering].[EndDate] AS [end_date],
            [dcol_offering_type].[Description] AS [offering_type],
            [dcol_site].[Description] AS [site_name],
            [dcol_fee].[FeeTypeName] AS [fee_type_name],
            [dcol_fee].[Amount] AS [amount],
            [dcol_offering].[CourseInformationID] AS [course_information_id],
            [dcol_offering].[UserDefined2] AS [university_link],
            [dcol_offering].[UserDefined3] AS [meta_title],
            [dcol_offering].[UserDefined4] AS [meta_desc],
            [dcol_offering].[UserDefined5] AS [video]

            FROM [Offering] as [dcol_offering]

            -- GET THE FEES FOR THE OFFERING
            LEFT JOIN (
                SELECT
                [dcol_offering_fee].[Amount],
                [dcol_offering_fee].[StudentFeeTypeID],
                [dcol_offering_fee].[OfferingID],
                [dcol_fee_type].[Description] AS [FeeTypeName]

                FROM [OfferingFee] as [dcol_offering_fee]

                -- MATCH THE FEES TO THE FEE TYPE NAME
                LEFT JOIN (
                    SELECT  
                        [FeeType].[FeeTypeID],
                        [FeeType].[Description]
                    FROM [FeeType]
                    WHERE [FeeType].[Description] = 'Tuition'
                ) AS [dcol_fee_type] ON [dcol_offering_fee].[FeeTypeID] = [dcol_fee_type].[FeeTypeID]

            ) AS [dcol_fee] ON [dcol_offering].[OfferingID] = [dcol_fee].[OfferingID]


            -- GET THE STATUS FOR THE OFFERING
            LEFT JOIN [OfferingStatus] AS [dcol_offering_status] ON [dcol_offering].[OfferingStatusID] = [dcol_offering_status].[OfferingStatusID]


            -- GET THE TYPE FOR THE OFFERING
            LEFT JOIN [OfferingType] AS [dcol_offering_type] ON [dcol_offering].[OfferingTypeID] = [dcol_offering_type].[OfferingTypeID]

            -- GET THE Site Name
            LEFT JOIN [Site] AS [dcol_site] ON [dcol_offering].[SiteID] = [dcol_site].[SiteID]

            -- GET THE Subject
            LEFT JOIN [CollegeLevel] AS [dcol_subject] ON [dcol_offering].[SID] = [dcol_subject].[SID]

            -- GET THE Parent Subject
            LEFT JOIN [CollegeLevel] AS [dcol_parent_subject] ON [dcol_subject].[ParentSID] = [dcol_parent_subject].[SID]

            -- GET THE Qualification Title
            LEFT JOIN [Learning_Aim] AS [dcol_qualification] ON [dcol_qualification].[LEARNING_AIM_REF] = [dcol_offering].[QualID]

            -- GET THE Awarding Body
            LEFT JOIN [AwardingBody] AS [dcol_awarding_body] ON [dcol_awarding_body].[AwardingBodyID] = [dcol_offering].[AwardingBodyID]

            -- GET THE Nice Name
            LEFT JOIN [CourseInformation] AS [dcol_course_info] ON [dcol_course_info].[CourseInformationID] = [dcol_offering].[CourseInformationID]

            WHERE
                [dcol_offering].[WebSiteAvailabilityID] <> 0
                -- AND ([dcol_offering].[AcademicYearID] = '18/19' OR [dcol_offering].[AcademicYearID] = '19/20')
        ";

        echo 'Attempting Connection to MSSQL DB...'. PHP_EOL;
        //Grab the courses from MSSQL

        try {
            $courses = DB::connection('mssql_sqlsrv')->select($sql);
        } catch (\Exception $e) {

            Mail::raw("Urgent Action Required!\n\nThere has been an issue with the course import. Please login and review.", function ($message) {
                $message->to('development@digitalallies.co.uk');
                $message->subject('CRITICAL FAILURE. ACTION REQUIRED - Darlington College API');
            });
            die ('Could not connection to DB. Please review information.' . PHP_EOL);
        }
        
        echo 'Connection Successful!' . PHP_EOL;

        //loop through all the courses and add a slug value to them based on the name.
        //also set the timestamps, and course type based on the CODE value.
        foreach($courses as $course) {

            //generate the slug (url path) based on the name of the course
            $name = $course->name;
            $slug = preg_replace('/[^A-Za-z0-9]/', '-', strtolower( $name ) );

            //add custom slug value to course item
            $course->slug = $slug;
            $course->created_at     = Carbon::now('utc')->toDateTimeString();
            $course->updated_at     = Carbon::now('utc')->toDateTimeString();
            $course->course_area    = str_replace(',','',$course->course_area);
            $course->course_type    = $this->get_course_type($course->code);

            if($course->course_type == 'Unknown') {
                if( strpos($course->name, 'GCSE') !== false ) {
                    $course->course_type = 'GCSE';
                }else if( strpos($course->name, 'Functional Skills') ){
                    $course->course_type = 'Functional Skills';
                }
            }
            
            $course->course_level   = $this->get_course_level($course->code);

        }

        //return array
        if( count($courses) > 0 ) {
        
            DB::table('courses')->truncate();
            $result = [];
            foreach($courses as $course) {
                array_push($result, get_object_vars($course) );
            }

            //insert the records into the local DB
            DB::table('courses')->insert(
                $result
            );

            echo  'Success! ' . count($result) . ' records have been successfully imported' . PHP_EOL;

        }else{
            echo  'Failure! courses have not been updated. Please re run manually.' . PHP_EOL;
            Mail::raw("Urgent Action Required!\n\nThere has been an issue with the course import. Please login and review.", function ($message) {
                $message->to('development@digitalallies.co.uk');
                $message->subject('CRITICAL FAILURE. ACTION REQUIRED - Darlington College API');
            });
        }

    }
    //Get the course type based on the offering CODE. The 6th character in the CODE represents the course type e.g. Full Time.
    private function get_course_type($code) {

        // find 6th character
        $char = substr($code,5,1);

        //get the value from the 6th character
        switch($char) {
            case 'A':
                return 'Apprenticeship';
                break;
            case 'F':
                return 'Full Time';
                break;
            case 'P':
                return 'Part Time Funded';
                break;
            case 'Z':
                return 'Part Time Non Funded';
                break;
            case 'U':
                return 'University';
                break;
            case 'S':
                return 'Subcontracted - Cskills';
                break;  
            default:
                return 'Unknown';
                break; 
        }
    }

    private function get_course_level($code) {

        // find 6th character
        $char = substr($code,2,1);

        //get the value from the 6th character
        switch($char) {
            case '0':
                return 'Level 0';
                break;
            case '1':
                return 'Level 1';
                break;
            case '2':
                return 'Level 2';
                break;
            case '3':
                return 'Level 3';
                break;
            case '4':
                return 'Level 4';
                break;
            case '5':
                return 'Level 5';
                break;
            case '6':
                return 'Level 6';
                break;
            case '7':
                return 'Level 7';
                break;
            default:
                return 'Unknown';
                break; 
        }
    }
}
