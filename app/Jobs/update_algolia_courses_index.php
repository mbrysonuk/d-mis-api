<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\courses;
use App\course_information;
use Illuminate\Support\Facades\Mail;

class update_algolia_courses_index implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo 'Starting...'. PHP_EOL;
        $client = new \AlgoliaSearch\Client('35UBGSOG4V', '7809513a60b7f731a666e947df3665c0');
        $index = $client->initIndex('courses');
        //grab courses and format them as JSON.
        $courses = courses::all();
        //$courses = json_encode($courses);

        echo '# Found Courses: ' . $courses->count() . PHP_EOL;
        // Had to disable alogolia information pass through whilst on free plan.
        foreach($courses as $course) {
            //$cid = course_information::where('course_information_id', $course->course_information_id)->where('course_image', NULL)->get();
            //$cid->course_image = '';
            //$course->information = $cid;

            if(!empty($course->clean_name)) {
                $course->name = $course->clean_name;
            }

        }

        if($courses->count() > 0) {
            echo 'Course Information Added!'. PHP_EOL;

            //delete all records from Algolia
            // *This is required, to avoid sending thousands of requests on each update, reducing load and cost.
            $del = $index->clearIndex();

            //wait until the clear Index task has completed before inserting new records (to avoid potential overlap of inserting/deleting records)
            $index->waitTask($del['taskID']);
            echo 'Algolia Index Cleared.'. PHP_EOL;

            //mass add objects again.
            $index->addObjects($courses);

            echo 'Success, Algolia has been updated' . PHP_EOL;   
        } else {
            echo 'No courses found, avoided deleting Algolia index. Please manually review DB and try again.';
            Mail::raw("Urgent Action Required!\n\nThe Algolia update has failed. Please login and review.", function ($message) {
                $message->to('development@digitalallies.co.uk');
                $message->subject('ALGOLIA CRITICAL FAILURE. ACTION REQUIRED - Darlington College API');
            });
            //  Mail::to('matthew@digitalallies.co.uk'))->send();
        }

    }
}
