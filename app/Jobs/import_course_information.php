<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class import_course_information implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       //Construct the SQL query
        $sql = "

            SELECT
                -- GET COURSE INFORMATION
                --Course Information ID
                [dcol_course_info].[CourseInformationID] AS [course_information_id],
                -- --Offering ID
                -- [dcol_offering].[OfferingID] AS [offering_id],
                -- [dcol_offering].[Name] AS [course_title],
                
                -- Course Info Type (inner join nested type on type value)
                -- [dcol_course_info_type].[Description] AS [course_info_type],
                [dcol_course_info_image_type].[Description] AS [course_info_image_type],
                [dcol_course_info_text_type].[Description] AS [course_info_text_type],

                -- Course Info Description
                [dcol_course_info].[Description] AS [course_info_description],
                -- Course Info InformationText
                
                -- Course Info Image
                [dcol_course_image].[Image] AS [course_image],
                -- Course Info Desc / Image Description??

                [dcol_course_text].[InformationText] AS [course_text],
                -- Course Info Academic Year ID
                [dcol_course_info].[AcademicYearID] AS [academic_year_id],

                -- Course Info SID (category/level)?
                [dcol_subject].[LevelNum] AS [course_level],
                [dcol_subject].[Name] AS [course_area],
                [dcol_parent_subject].[Name] AS [parent_course_area],

                -- Course Info CATEGORIES?
                [dcol_course_info].[Categories] AS [categories],
                [dcol_course_text].[SortOrder] AS [sort_order]


            FROM [CourseInformation] AS [dcol_course_info]

            -- LEFT JOIN [Offering] AS [dcol_offering] ON [dcol_offering].[CourseInformationID] = [dcol_course_info].[CourseInformationID]

            FULL OUTER JOIN [CourseInformationImage] AS [dcol_course_image] ON [dcol_course_info].[CourseInformationID] = [dcol_course_image].[CourseInformationID]
            FULL OUTER JOIN [CourseInformationText] AS [dcol_course_text] ON [dcol_course_info].[CourseInformationID] = [dcol_course_text].[CourseInformationID]


            -- GET THE Subject
            LEFT JOIN [CollegeLevel] AS [dcol_subject] ON [dcol_course_info].[SID] = [dcol_subject].[SID]

            -- GET THE Parent Subject
            LEFT JOIN [CollegeLevel] AS [dcol_parent_subject] ON [dcol_subject].[ParentSID] = [dcol_parent_subject].[SID]


            -- get the type of course info from the text/image joins
            LEFT JOIN [CourseInformationType] AS [dcol_course_info_image_type] ON [dcol_course_info_image_type].[CourseInformationTypeID] = [dcol_course_image].[CourseInformationTypeID]

            LEFT JOIN [CourseInformationType] AS [dcol_course_info_text_type] ON [dcol_course_info_text_type].[CourseInformationTypeID] = [dcol_course_text].[CourseInformationTypeID]


            -- WHERE
            --     -- [dcol_offering].[WebSiteAvailabilityID] <> 0
            --     -- AND 
            --     [dcol_offering].[AcademicYearID] = '18/19'
        ";

        //Grab the courses from MSSQL
        $course_info = DB::connection('mssql_sqlsrv')->select($sql);

        //loop through all the coruses and add a slug value to them based on the name.
        foreach($course_info as $course_info_item) {

            // //generate the slug (url path) based on the name of the course
            // $name = $course_info_item->name;
            // $slug = preg_replace('/[^A-Za-z0-9]/', '-', strtolower( $name ) );

            //add custom slug value to course item
            //$course->slug = $slug;
            $course_info_item->created_at = Carbon::now('utc')->toDateTimeString();
            $course_info_item->updated_at = Carbon::now('utc')->toDateTimeString();

        }

        //return array
        DB::table('course_information')->truncate();
        $result = [];
        foreach($course_info as $course_info_item) {
            array_push($result, get_object_vars($course_info_item) );
        }

        //insert the records into the local DB
        DB::table('course_information')->insert(
            $result
        );

        echo  'Success! ' . count($result) . ' course information records have been successfully imported' . PHP_EOL;
    }
}
