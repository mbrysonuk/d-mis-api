<?php

namespace App\Http\Middleware;

use Closure;
use App\ApiKeys;

class CustomAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request_key = $request->apikey;
        $api_key_check = ApiKeys::where('key', $request_key )->first();
        
        //if the api is not found in the list of keys, kill the request.
        if( empty( $api_key_check ) ) {
            abort(403, "You Shall Not Pass!! \n (You do not have an active API key in your request.)");
        }

        return $next($request);
    }
}
