<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\application;
use App\courses;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApplicationController extends Controller
{
    public function __construct() {
        // $this->TableName    = "dc_courses";
        // //$this->CreateCoursesTable();

        // $this->dsn          = 'odbc:Driver=FreeTDS;SERVER=194.66.96.82;';
        // $this->user         = 'DigitalAlliesWrite';
        // $this->password     = '.$gw4Rpj[kJ8G}}+k';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Application Home';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //decode the request from the site.
        $data = json_decode( $request->getContent() );
        $result = get_object_vars($data);

        //store the messages for the response to be passed back via the API.
        $success = json_encode(['success' => 'Application Stored.']);
        $error = json_encode(['error' => 'There was a problem with your submission. Please try again or contact the server administrator.']); 

        //If the result is empty, return an error.
        // @todo Make this do further checks and provide greater insight in the error response e.g. check for required fields and validate input types.
        if(empty($result)) {
        return response($error)
            ->header('Content-Type', 'application/json');
        }

        //Store the status values that don't change, but are required by the PSWebEnrolment table
        $result['request_status']       = "To Do";
        $result['sent_marketing_info']  = "false";
        $result['is_full_time']         = "true";

        //Encode the qualifications as JSON. This is so we dont need to create a seperate Quals table locally.
        $result['qualifications']       = json_encode( $result["qualifications"] );
        
        //figure out if its full time application or not, based on offering code.
        //@todo Just pass this in the data from the site, the API is called there anyway!
        if(!empty($result['code'])) {
            if($result['code'] == 'Full Time') {
                $result['is_full_time'] = 'true';
            }else{
                $result['is_full_time'] = 'false';
            }
        }

        //create a new local application request record using the result from the request data.
        $application = application::create($result);

        //return a response to indicate successful import of application.    
        return response($success)
            ->header('Content-Type', 'application/json');

    }
}
