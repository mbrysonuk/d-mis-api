<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\courses;
use App\course_information;
use App\Jobs\import_courses;

class CourseController extends Controller
{

	function __construct() {

	}

	//get the course from the local database based on the offering ID
	public function get_course($offering_id) {
		//find the first course matching the offering ID. Offering ID's are unique, so only one result should ever be found. This avoids the result accidently returning multiple results.
		$course = Courses::where('offering_id', $offering_id)->first();

		//encode an error message to be used should the method fail.
		$error = json_encode(['error' => 'course not found. Please check offering id and try again.']);	

		//check to see if a result was returned in the course retrival above. If not, return an error in the response, ending the methods current run.
		if(empty($course)) {
			return response($error)
		   		->header('Content-Type', 'application/json')
		   		->header('Status', '400');
		}

		// check to see if the course has a clean name associated with it, and use that as the primary name if so. Courses only have clean names when they are tied to a course information record.
		if(!empty($course->clean_name)) {
			$course->name = $course->clean_name;
		}

		//get the course information based on the course > course information ID reference.
		$cid = $course->course_information_id;
		//get all course information items related to the course. There are often multiple course information records tied to a single course.
		$course_information = course_information::where('course_information_id', $cid)->get();

		//This is a quick fix for how images are stored. It was causing issues in the JSON response, so it has to be removed until we can ensure images will place nice.
		foreach($course_information as $ci) {
			$ci->course_image = '';
		}

		//store the course information array of items as part of the primary course record.
		$course->information = $course_information;

		//return the course item and course information, formatted as JSON.
		return response($course)
   			->header('Content-Type', 'application/json');

	}


	//Get all the courses for a particular category, known as Course Area in our local DB, and Subject in PSWebEnrolment
	public function get_courses($category_id) {
		//use the private find category function to return the category name for searching offerings in the local table.
		$cat = $this->find_category($category_id);

		//checks to see if the category is found before attempting to get the categories courses.
		if(!empty($cat)) {
			$courses = Courses::where('course_area',$cat)->get();
		} else {
			return 'Error, Category not found.';
		}

		//store a generic error message as json.
		$error = json_encode(['error' => 'course not found. Please check offering id and try again.']);

		//if no courses are returned, retun an error message as the response.
		if(empty($courses)) {
			return response($error)
		   		->header('Content-Type', 'application/json')
		   		->header('Status', '400');
		}

		// check to see if the course has a clean name associated with it, and use that as the primary name if so. Courses only have clean names when they are tied to a course information record.
		foreach($courses as $course) {
			if(!empty($course->clean_name)) {
				$course->name = $course->clean_name;
			}
		}

		//return the course items and course information, formatted as JSON.
		return response($courses)
   			->header('Content-Type', 'application/json');

	}


	//internal function for converting cateogry id codes to category names when searching the courses table for relevant category results.
	private function find_category($cat) {

		switch($cat) {

			case 'BA':	
				return 'SERVICE ENTERPRISES';
				break;

			case 'CA':
				return 'BUILDING AND CONSTRUCTION';
				break;

			case 'DA':
				return 'CRAFTS, CREATIVE ART AND DESIGN';
				break;

			case 'DB':
				return 'MEDIA AND COMMUNICATION';
				break;

			case 'DC':
				return 'COMPUTING';
				break;

			case 'DD':
				return 'TRAVEL AND TOURISM';
				break;

			case 'EA':
				return 'ENGINEERING';
				break;

			case 'EB':
				return 'MANUFACTURING TECHNOLOGIES';
				break;

			case 'EC':
				return 'TRANSPORTATION OPERATIONS AND MAINTENANCE';
				break;

			case 'ED':	
				return 'SCIENCE';
				break;

			case 'HA':	
				return 'HEALTH AND SOCIAL CARE';
				break;

			case 'HB':
				return 'CHILDCARE';
				break;

			case 'MA':	
				return 'FOUNDATIONS FOR LEARNING AND LIFE';
				break;

			case 'MB':
				return 'MATHEMATICS AND STATISTICS';
				break;

			case 'MC':
				return 'TEACHING AND LECTURING';
				break;

			case 'MD':
				return 'MOD';
				break;

			case 'ME':
				return 'BUSINESS, ADMINISTRATION AND LAW';
				break;

			case 'SA':	
				return 'SPORT, LEISURE AND RECREATION';
				break;

			case 'SB':	
				return 'PUBLIC SERVICES';
				break;

			case 'SC':
				return 'HOSPITALITY AND CATERING';
				break;

			case 'FOUNDLEARN':
				return 'FOUNDATION LEARNING';
				break;

			case 'MBLLC':
				return 'LANGUAGES, LITERATURE AND CULTURE';
				break;


			case 'PAIS':
				return 'PUBLISHING AND INFORMATION SERVICES';
				break;

			case 'EDEA':
				return 'ENGINEERING AND SCIENCE';
				break;

			case 'OPTSK':
				return 'OPTIMUM SKILLS LIMITED';
				break;

			default:
				return '';
				break;

		}
	}
}
