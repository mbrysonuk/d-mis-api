<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class application extends Model
{
    protected $fillable = [
    	'title',
    	'first_name',
    	'surname',
    	'date_of_birth',
    	'address_line_1',
    	'address_line_2',
    	'address_line_3',
        'postcode',
    	'country',
    	'tel',
    	'mobile_tel',
    	'email',
    	'ethnicity',
    	'offering_1',
    	'last_school',

        'qualifications',
        
        'have_employer',
        'employer_name',
        'employer_email',
        'employer_tel',

        'referee_1_name',
        'referee_1_relationship',
        'referee_1_email',
        'referee_1_tel',
        'referee_2_name',
        'referee_2_relationship',
        'referee_2_email',
        'referee_2_tel',

        'interview_support',
        'learning_support',
        'any_other_support',
        'any_other_support_details',
        'local_authority_care',
        'further_support_info',

        'request_date',
        'request_status',
        'academic_year_id',
        'is_full_time',
        'site_application_id',
    ];
}
