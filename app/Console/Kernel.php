<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\import_course_command',
        'App\Console\Commands\update_algolia_command',
        'App\Console\Commands\push_applications_to_ps_command'
        //'App\Console\Commands\import_course_command'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //pulls in courses from Pro Solution
        $schedule->command('dcol:import_ps_courses')->cron('0 */8 * * *');
        //Pushes courses to Algolia
        $schedule->command('dcol:update_algolia_courses')->dailyAt('00:15');
        //Pushes Applications to Pro Solution
        $schedule->command('dcol:push_applications_to_ps')->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
