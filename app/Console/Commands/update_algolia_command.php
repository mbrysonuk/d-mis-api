<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\update_algolia_courses_index;

class update_algolia_command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcol:update_algolia_courses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the local DB copy of the courses to Asanas index';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        update_algolia_courses_index::dispatch();
    }
}
