<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\import_courses;
use App\Jobs\import_course_information;

class import_course_command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcol:import_ps_courses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import courses and course information from Pro Solution';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        import_courses::dispatch();
        import_course_information::dispatch();


    }
}
