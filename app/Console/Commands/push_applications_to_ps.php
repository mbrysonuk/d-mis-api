<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\push_applications_to_ps;


class push_applications_to_ps_command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcol:push_applications_to_ps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push unsent applications to PSWebEnrolment DB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        push_applications_to_ps::dispatch();
    }
}
