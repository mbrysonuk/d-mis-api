@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1>Application Start Page - Page {{ $stage }}</h1>

<form action="/application/{{ $stage + 1 }}" method="POST" >
    @csrf
    <div class="accordion course-details">
        <div class="form-group has-false has-success">
            <label for="inputTitle" class="control-label col-sm-3">Title*</label>
            <div class="col-sm-9">
                <select class="form-control ng-dirty ng-valid ng-valid-required" ng-model="formData.personal_info.title" required="">
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Miss">Miss</option>
                    <option value="Ms">Ms</option>
                </select>
            </div>
        </div>

        <div class="form-group has-false">
            <label for="inputFirstName" class="control-label col-sm-3">First Name*</label>
            <div class="col-sm-9">
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required">
            </div>
        </div>

        <div class="form-group has-false">
            <label for="inputLastName" class="control-label col-sm-3">Last Name*</label>
            <div class="col-sm-9">
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.personal_info.last_name" required="">
            </div>
        </div>

        <div ng-controller="dobController" class="ng-scope">
            <div class="form-group has-success">
                <label for="inputDOB" class="control-label col-sm-3">Date Of Birth*</label>


                <div class="col-sm-3">
                    <select name="" class="form-control ng-pristine ng-valid" ng-options="day for day in days" ng-model="formData.dob.day">
                        <option value="0" selected="selected">Day</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                </div>


                <div class="col-sm-3">
                    <select name="" class="form-control ng-pristine ng-valid" ng-model="formData.dob.month">
                        <option value="0">Month</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>


                <div class="col-sm-3">
                    <select name="" class="form-control ng-pristine ng-valid" ng-model="formData.dob.year">
                        <option value="0">Year</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                        <option value="1969">1969</option>
                        <option value="1968">1968</option>
                        <option value="1967">1967</option>
                        <option value="1966">1966</option>
                        <option value="1965">1965</option>
                        <option value="1964">1964</option>
                        <option value="1963">1963</option>
                        <option value="1962">1962</option>
                        <option value="1961">1961</option>
                        <option value="1960">1960</option>
                        <option value="1959">1959</option>
                        <option value="1958">1958</option>
                        <option value="1957">1957</option>
                        <option value="1956">1956</option>
                        <option value="1955">1955</option>
                        <option value="1954">1954</option>
                        <option value="1953">1953</option>
                        <option value="1952">1952</option>
                        <option value="1951">1951</option>
                        <option value="1950">1950</option>
                        <option value="1949">1949</option>
                        <option value="1948">1948</option>
                        <option value="1947">1947</option>
                        <option value="1946">1946</option>
                        <option value="1945">1945</option>
                        <option value="1944">1944</option>
                        <option value="1943">1943</option>
                        <option value="1942">1942</option>
                        <option value="1941">1941</option>
                        <option value="1940">1940</option>
                        <option value="1939">1939</option>
                        <option value="1938">1938</option>
                        <option value="1937">1937</option>
                        <option value="1936">1936</option>
                        <option value="1935">1935</option>
                        <option value="1934">1934</option>
                        <option value="1933">1933</option>
                        <option value="1932">1932</option>
                        <option value="1931">1931</option>
                        <option value="1930">1930</option>
                        <option value="1929">1929</option>
                        <option value="1928">1928</option>
                        <option value="1927">1927</option>
                        <option value="1926">1926</option>
                        <option value="1925">1925</option>
                        <option value="1924">1924</option>
                        <option value="1923">1923</option>
                        <option value="1922">1922</option>
                        <option value="1921">1921</option>
                        <option value="1920">1920</option>
                        <option value="1919">1919</option>
                        <option value="1918">1918</option>
                        <option value="1917">1917</option>
                        <option value="1916">1916</option>
                        <option value="1915">1915</option>
                        <option value="1914">1914</option>
                        <option value="1913">1913</option>
                        <option value="1912">1912</option>
                        <option value="1911">1911</option>
                        <option value="1910">1910</option>
                        <option value="1909">1909</option>
                    </select>
                </div>
            </div>
                <div class="form-group has-false ng-hide" ng-show="formData.age > 0">
                    <label for="inputDOB" class="control-label col-sm-3">Confirm Age</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label class="ng-binding">
                                <input type="checkbox" ng-model="formData.confirm_age" value="true" required="" class="ng-pristine ng-invalid ng-invalid-required"> I confirm I am  years old
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group has-false">
                <label for="inputGender" class="control-label col-sm-3">Gender*</label>
                <div class="col-sm-3">
                    <select id="inputGender" name="gender" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.personal_info.gender" required="">
                        <option value="? undefined:undefined ?"></option>
                        <option value="Please Select">Please Select</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>
            <fieldset>
                <legend>Address &amp; Contact Details</legend>
                <div class="form-group has-false">
                    <label for="inputAddressLine1" class="control-label col-sm-3">Line 1*</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.contact_details.address.line1" required="">
                    </div>
                </div>  
                <div class="form-group has-false">
                    <label for="inputAddressLine2" class="control-label col-sm-3">Line 2</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control ng-pristine ng-valid" ng-model="formData.contact_details.address.line2">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputAddressLine2" class="control-label col-sm-3">Town*</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.contact_details.address.town" required="">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputPostcode" class="control-label col-sm-3">Postcode*</label>
                    <div class="col-sm-9">
                        <input id="inputPostcode" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.contact_details.address.postcode" required="">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputContactNumber" class="control-label col-sm-3">Contact Telephone Number*</label>
                    <div class="col-sm-9">
                        <input id="inputContactNumber" type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.contact_details.telephone" required="">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputMobileNumber" class="control-label col-sm-3">Mobile Telephone Number*</label>
                    <div class="col-sm-9">
                        <input id="inputMobileNumber" type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.contact_details.mobile" required="">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputEmail" class="control-label col-sm-3">Email Address*</label>
                    <div class="col-sm-5">
                        <input id="inputEmail" type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" ng-model="formData.contact_details.email" required="">
                    </div>
                    <div class="col-sm-4">
                        <p class="form-control-static" style="font-size: 14px;">
                            <a href="" data-target="#no-email" data-toggle="modal">Don't have an email address?</a>
                        </p>
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputPostcode" class="control-label col-sm-3">Are you a resident of the UK, EU or EUA?</label>
                    <div class="col-sm-3">
                        <label class="radio-inline">
                            <input type="radio" id="inlineCheckbox1" name="ukresident" ng-model="formData.personal_info.uk_resident" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="inlineCheckbox2" name="ukresident" ng-model="formData.personal_info.uk_resident" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                        </label>
                    </div>
                    
                </div>
                <div class="form-group ng-hide has-false" ng-show="formData.personal_info.uk_resident == 'no'">
                    <label class="control-label col-sm-3">Date you entered the UK</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control ng-pristine ng-valid ng-valid-required" ng-model="formData.personal_info.date_entered" placeholder="dd/mm/yyyy" ng-required="formData.personal_info.uk_resident == 'no'">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputPostcode" class="control-label col-sm-3">Ethnic Origin:</label>
                    <div class="col-sm-9">
                        <select class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="formData.personal_info.ethnicity" required=""><option value="? undefined:undefined ?"></option>
                            <option value="A">White English/Welsh/Scottish/Northern Irish/British</option>
                            <option value="B">White Irish</option>
                            <option value="C">White Gypsy or Irish Traveller</option>
                            <option value="D">Any other white background</option>
                            <option value="E">White and Black Caribbean</option>
                            <option value="F">Bangladeshi</option>
                            <option value="G">Chinese</option>
                            <option value="H">Any other Asian background</option>
                            <option value="I">African</option>
                            <option value="J">Caribbean</option>
                            <option value="K">White and Black African</option>
                            <option value="L">White and Asian</option>
                            <option value="M">Any other mixed/multiple ethnic background</option>
                            <option value="N">Indian</option>
                            <option value="O">Pakistani</option>
                            <option value="P">Any other Black/African/Caribbean background</option>
                            <option value="Q">Arab</option>
                            <option value="R">Any other Ethnic group</option>
                            <option value="S">Not known/not provided</option>
                        </select>
                    </div>
                </div>
            </fieldset>
            <input type="submit" class="btn btn-primary pull-right" value="Next">
        </div>


        <div class="accordion course-details">
            <div class="form-group">
                <label for="inputPostcode" class="control-label col-sm-4">Your chosen course:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">Level 3 Extended Diploma - * Extended Diploma Creative Digital Media Production  -  (Full time)</p>
                                </div>
            </div>
            <div class="form-group has-false">
                <label for="inputLastName" class="control-label col-sm-4">Why have you chosen this course?*:</label>
                <div class="col-sm-8">
                    <textarea class="form-control ng-pristine ng-invalid ng-invalid-required" rows="3" name="course_details[choice_reasoning]" ng-model="formData.course_details.choice_reasoning" required=""></textarea>
                </div>
            </div>

            <div class="form-group has-false">
                <label for="inputPostcode" class="control-label col-sm-4">Have you previously been a student at Darlington College?*:</label>
                <div class="col-sm-8">
                    <label class="radio-inline">
                        <input type="radio" id="inlineCheckbox1" name="course_details[previous_student]" ng-model="formData.course_details.previous_student" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" id="inlineCheckbox2" name="course_details[previous_student]" ng-model="formData.course_details.previous_student" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                    </label>
                </div>
            </div>
            <div ng-show="formData.course_details.previous_student == 'yes'" class="ng-hide">
                <div class="form-group has-false">
                    <label for="inputPreviousStudent" class="control-label col-sm-4">Course*</label>
                    <div class="col-sm-8">
                        <input id="inputPreviousStudent" type="text" name="course_details[previous_course]" ng-model="formData.course_details.previous_course" ng-required="formData.course_details.previous_student == 'yes'" class="form-control ng-pristine ng-valid ng-valid-required">
                    </div>
                </div>
                <div class="form-group has-false">
                    <label for="inputPreviousTutor" class="control-label col-sm-4">Tutor*</label>
                    <div class="col-sm-8">
                        <input id="inputPreviousTutor" type="text" name="course_details[previous_tutor]" ng-model="formData.course_details.previous_tutor" ng-required="formData.course_details.previous_student == 'yes'" class="form-control ng-pristine ng-valid ng-valid-required">
                    </div>
                </div>
            </div>
            <input type="submit" class="btn btn-primary pull-right" value="Next">
        </div>


    <div class="accordion your-qualifications">
        <fieldset>
            <legend>College/School you most recently attended</legend>
            <div class="form-group has-false">
                <label for="inputPostcode" class="control-label col-sm-3">Name*:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" name="recent_college[name]" ng-model="formData.recent_college.name" required="" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                </div>
            </div>
            <div class="form-group has-false">
                <label for="inputAddress" class="control-label col-sm-3">Address*:</label>
                <div class="col-sm-8">
                    <input type="text" id="inputAddress" class="form-control ng-pristine ng-invalid ng-invalid-required" name="recent_college[address]" ng-model="formData.recent_college.address" required="">
                </div>
            </div>
            <div class="form-group has-false">
                <label for="inputPostcode" class="control-label col-sm-3">Website*:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" name="recent_college[website]" ng-model="formData.recent_college.website" required="">
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Your Qualifications</legend>
            <div ng-show="formData.confirm_age" class="ng-hide">
                <p>
                    Please enter the GCSE / A Level qualifications you have that are most appropriate to the 
                    course you are applying for. 
                </p>
                <blockquote class="entry-requirements">
                    <p>
                        You will need to have a pass at Foundation Diploma level.                   </p>
                </blockquote>
                <div class="col-sm-s12">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <label>Subject</label>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label>Level</label>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label>Grade</label>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label>Year Achieved</label>
                        </div>
                    </div>
                    <div ng-controller="qualificationController" class="ng-scope">
                        <!-- ngRepeat: qualification in formData.qualifications --><div class="qualification-row form-group ng-scope" ng-repeat="qualification in formData.qualifications">
                            <div class="col-md-5 col-sm-5">
                                <input type="text" ng-model="qualification.subject" class="form-control ng-pristine ng-valid">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" ng-model="qualification.level" class="form-control ng-pristine ng-valid">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" ng-model="qualification.grade" class="form-control ng-pristine ng-valid">
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <input type="text" ng-model="qualification.date" class="form-control ng-pristine ng-valid">
                            </div>
                            <span class="glyphicon glyphicon-remove ng-hide" ng-click="removeQualification($index)" ng-show="!qualification.mandatory"></span>
                        </div><!-- end ngRepeat: qualification in formData.qualifications --><div class="qualification-row form-group ng-scope" ng-repeat="qualification in formData.qualifications">
                            <div class="col-md-5 col-sm-5">
                                <input type="text" ng-model="qualification.subject" class="form-control ng-pristine ng-valid">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" ng-model="qualification.level" class="form-control ng-pristine ng-valid">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" ng-model="qualification.grade" class="form-control ng-pristine ng-valid">
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <input type="text" ng-model="qualification.date" class="form-control ng-pristine ng-valid">
                            </div>
                            <span class="glyphicon glyphicon-remove ng-hide" ng-click="removeQualification($index)" ng-show="!qualification.mandatory"></span>
                        </div><!-- end ngRepeat: qualification in formData.qualifications -->
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="button" ng-click="addQualification()" class="btn btn-default pull-right">Add another qualification</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div ng-show="!formData.confirm_age" class="">
                <p>You must confirm your age in the <a href="#/step/1">Personal info</a> section before you can add any qualifications</p>
            </div>
        </fieldset>
        <input type="submit" class="btn btn-primary pull-right" value="Next">
    </div>



    <div class="accordion learning-support">
        <div class="form-group has-false">
            <label for="inputPostcode" class="control-label col-sm-9">Do you require any support or special arrangements at interview:</label>
            <div class="col-sm-3">
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox1" name="learning_support[interview_support]" ng-model="formData.learning_support.interview_support" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox2" name="learning_support[interview_support]" ng-model="formData.learning_support.interview_support" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                </label>
            </div>
        </div>
        <div class="form-group has-false">
            <label for="inputPostcode" class="control-label col-sm-9">Do you need any support with your learning?:</label>
            <div class="col-sm-3">
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox1" name="learning_support[study_support]" ng-model="formData.learning_support.study_support" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox2" name="learning_support[study_support]" ng-model="formData.learning_support.study_support" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                </label>
            </div>
        </div>
        <div class="form-group has-false">
            <label for="inputPostcode" class="control-label col-sm-9">Do you need any support with anything else eg; personal issues, health, settling into college or any other pastoral need?:</label>
            <div class="col-sm-3">
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox1" name="learning_support[personal_mentoring]" ng-model="formData.learning_support.personal_mentoring" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox2" name="learning_support[personal_mentoring]" ng-model="formData.learning_support.personal_mentoring" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                </label>
            </div>
        </div>
        <div class="form-group ng-hide has-false" ng-show="formData.learning_support.personal_mentoring == 'yes'">
            <label for="inputMentoringInfo" class="col-sm-12">Details of the support you require</label>
            <div class="col-sm-12">
                <textarea class="form-control ng-pristine ng-valid ng-valid-required" ng-required="formData.learning_support.personal_mentoring == 'yes'" ng-model="formData.learning_support.personal_mentoring_details" rows="3"></textarea>
            </div>
        </div>
        <div class="form-group has-false">
            <label for="inputPostcode" class="control-label col-sm-9">Are you under the care of the Local Authority?:</label>
            <div class="col-sm-3">
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox1" name="learning_support[looked_after]" ng-model="formData.learning_support.looked_after" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox2" name="learning_support[looked_after]" ng-model="formData.learning_support.looked_after" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                </label>
            </div>
        </div>
        <p>
            The college is able to assist students with a range of financial 
            support initiatives depending upon personal circumstances. This 
            includes assistance with kits, uniforms, transport, and childcare.
        </p>
        <div class="form-group has-false">
            <label for="inputPostcode" class="control-label col-sm-9">Would you like further information on financial support available?:</label>
            <div class="col-sm-3">
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox1" name="learning_support[financial_support]" ng-model="formData.learning_support.financial_support" value="yes" required="" class="ng-pristine ng-invalid ng-invalid-required"> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" id="inlineCheckbox2" name="learning_support[financial_support]" ng-model="formData.learning_support.financial_support" value="no" required="" class="ng-pristine ng-invalid ng-invalid-required"> No
                </label>
            </div>
        </div>
        <hr>
        <p>
            Information provided on this application form will be shared with 
            relevant Darlington College staff members at the appropriate time in 
            order for us to put the required support into place. This may include 
            our Additional Learning Support Team, tutors, and relevant support staff.
        </p>
        <a href="" class="btn btn-success pull-right" data-target="#submit-application-dialog" data-toggle="modal">Finish Application</a>
    </div>




</form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
