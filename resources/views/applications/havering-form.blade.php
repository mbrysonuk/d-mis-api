@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1>Application Start Page - Page {{ $stage }}</h1>

<form action="/application/{{ $stage + 1 }}" method="POST" >
    @csrf

    <div class="form-group">

    	<label for="Title">*Title</label>
    	<input name="Title" type="text">

    	<label for="forename">*Forename(s)</label>
    	<input name="forename" type="text">

    	<label for="Surname">*Surname</label>
    	<input name="Surname" type="text">

    	<label for="DateOfBirth">Date Of Birth</label>
    	<input name="DateOfBirth" type="text">

    </div>

<!--     <div class="form-group">
    	<label for="known_as">Known As</label>
    	<input name="known_as" type="text">
    </div> -->

<!--     <div class="form-group">
    	<label for="surname">Known As</label>
    	<input name="surname" type="text">
    </div> -->




    <div class="form-group">
    	<label for="title">*Gender</label>
    	<input name="title" type="text">
    </div>  


    <div class="form-group">
    	<label for="">Learner Number (for previous students, if known)</label>
    	<input name="" type="text">
    </div>  

    <div class="form-group">
    	<label for="Address1">Address Line 1</label>
    	<input name="Address1" type="text">

    	<label for="Address2">Address Line 2</label>
    	<input name="Address2" type="text">

    	<label for="Address3">Address Line 3</label>
    	<input name="Address3" type="text">

    	<label for="Address4">Address Line 4</label>
    	<input name="Address4" type="text">

    	<label for="Postcode">Postcode</label>
    	<input name="Postcode" type="text">


CountryID

    	<label for="Postcode">Postcode</label>
    	<input name="Postcode" type="text">

    </div>  


*Current Address (, Address Line 2, Town, County)
*Postcode
*Country
*Email
*Email (enter again)
*Telephone
Mobile
*First Language
*Ethnicity
*Have you been resident in the EU for the past 3 years?
Do you have a learning difficulty or disability (primary)?
Do you have a learning difficulty or disability (secondary or other)?
Please state any other medical condition or physical disability
Are you in the care system?
*Do you have any current or previous criminal convictions?
Do you require additional support for examinations?




</form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php /*
************** PAGE 1 - PERSONAL DETAILS ***************
*Surname
*Forename(s)
Known As
*Title
*Gender
Date Of Birth
Learner Number (for previous students, if known)
*Current Address (Address Line 1, Address Line 2, Town, County)
*Postcode
*Country
*Email
*Email (enter again)
*Telephone
Mobile
*First Language
*Ethnicity
*Have you been resident in the EU for the past 3 years?
Do you have a learning difficulty or disability (primary)?
Do you have a learning difficulty or disability (secondary or other)?
Please state any other medical condition or physical disability
Are you in the care system?
*Do you have any current or previous criminal convictions?
Do you require additional support for examinations?


************** PAGE 2 - FURTHER DETAILS ***************
Please give details of your most recent school or college
Please give details of your most recent employer (if applicable)
How did you find out about this course?
"
If you have applied to study at the college as an on-campus full time or part time student please complete a short statement in support of your application, detailing your reasons for applying to the course and your interest in the subject area. This statement may form part of your interview so please include all relevant information.
If you have applied for an adult online course you will not need an interview but please state here your interest in applying. Our Online Learning team will be in touch with you to discuss induction arrangements.
"

************** PAGE 3 - QUALIFICATIONS ON ENTRY ***************
What is your highest level of qualification?
"
Please list here your other qualifications or predicted grades if you have not yet taken your exams. You will need to add each subject individually. Include information on all qualifications i.e. GCSE, NVQs, BTEC, Diplomas, A levels or any other subjects you may have studied.
"
{
	Qualification,
	Subject (if not in list)
	Grade
	Predicted Grade
	Date Awarded
}

************** PAGE 4 - REFERENCES **************
No References
{
	Referee Surname,
	Referee Forename,
	Tel,
	Email,
	Relationship to you,
	Referee Job Title
}

************** PAGE 5 - Review & Submit **************
College Declaration - I confirm that I have read the declaration above
Create Account? (password, retype password)
*/?>
