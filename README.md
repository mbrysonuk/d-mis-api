API for ProSolution interactions.

ProSolution is a college courses and application management portal. The client in this example used this software for handling all applications manually.

I created a solution that helped them solve two issues;

1. Management of applications
2. Management of course information

First, I looked to automate the workflow for applications as they came in. The solution was to create a single endpoint that accepts application data from
a form (in this example, from WordPress, although the platform isn't important!). One the data is recieved, it is manipulated for use with ProSolution and
stored in a local database, which is then used to pass the data to ProSolution at set intervals. It is first stored locally to avoid any potential issues
with the final stage of the data transfe, as Pro Solution is often hosted on internal college servers and is at risk of failure at any time. The data is 
manipulated in within this app to avoid requiring strict rules at the point of data creation, minising the risk of applications failing on initial import.

Second, I looked into how to easily take course information from the internal ProSolution system and make it available quickly on demand. The solution was
to create a dynamic endpoint that accepted a course ID and returns the related course information. ProSolution stores its data in an MSSQL database in such
a way that isnt easy to retrieve, so some complex SQL commands were required to pull the data into an easily usable format. A backup of every course is stored
locally on this app, and updated at set intervals. This means the websites calling the endpoint are not relying on ProSolution - if it goes down, my app 
still has its most recent backup in place, and the developer can work on fixing the failed ProSolution sync whilst users are not affected in any way. This
failsafe was added because ProSolution has a tendency to change its DB structure on update without prior warning, causing major headaches if prior preparation
isnt set up!

this is a fairly simple breakdown, the app actually does a little bit more than this, but the above description gives an idea of the solution i wanted to
achieve. This system is now used on a daily basis for the client, and has saved them countless hours in administrative tasks. Although i wont mention the
name here, check out my recommendations on linkedin :)

I really like how the app turned out for its first phase. I had discussed a second phase with the client to implement a application management portal for
students, allowing them to view the progress of an application and submit dates for interviews. The app was built with this in mind.

Data retention was also another factor under consideration, ensuring data held is only held until its not needed anymore.



